use std::{
    fmt::{Debug, Display},
    net::UdpSocket,
};

pub type Bytes = Vec<u8>;

pub const TYPE_A: u16 = 1;
pub const CLASS_IN: u16 = 1;
pub const RECURSION_DESIRED: u16 = 1 << 8;
pub const NO_RECURSION: u16 = 0;

//-************************************************************************
// main public interface, build_query and send_query
//-************************************************************************

pub fn build_query(name: &str, record_type: u16, id: u16, flags: u16) -> Bytes {
    let name = encode_name(name);

    let header = DNSHeader {
        id,
        flags,
        questions: 1,
        ..Default::default()
    }
    .to_bytes();

    let mut question = DNSQuestion {
        encoded_name: name,
        tipe: record_type,
        class: CLASS_IN,
    }
    .to_bytes();

    let mut query = header;
    query.append(&mut question);

    query
}

pub fn send_query(query: &[u8], server_ip: &str) -> DNSPacket {
    let socket = UdpSocket::bind("0.0.0.0:8829").unwrap();
    let mut buf = [0u8; 1024];
    let _sent = socket.send_to(query, (server_ip, 53)).unwrap();
    let (_recvd, _addr) = socket.recv_from(&mut buf).unwrap();
    parse_response(&buf)
}

//-************************************************************************
// DNS structs
//-************************************************************************

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct DNSHeader {
    pub id: u16,
    pub flags: u16,
    pub questions: u16,
    pub answers: u16,
    pub authorities: u16,
    pub additionals: u16,
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct DNSQuestion {
    pub encoded_name: Bytes,
    pub tipe: u16,
    pub class: u16,
}

#[derive(Default, Clone, PartialEq, Eq)]
pub struct DNSRecord {
    pub name: String,
    tipe: u16,
    class: u16,
    ttl: u32,
    data: Bytes,
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct DNSPacket {
    header: DNSHeader,
    questions: Vec<DNSQuestion>,
    answers: Vec<DNSRecord>,
    authorities: Vec<DNSRecord>,
    additionals: Vec<DNSRecord>,
}

//-************************************************************************
// trait impls
//-************************************************************************

trait Bytable {
    fn to_bytes(&self) -> Bytes;
}

impl Bytable for DNSHeader {
    fn to_bytes(&self) -> Bytes {
        let mut ret = vec![];
        append_u16(&mut ret, self.id);
        append_u16(&mut ret, self.flags);
        append_u16(&mut ret, self.questions);
        append_u16(&mut ret, self.answers);
        append_u16(&mut ret, self.authorities);
        append_u16(&mut ret, self.additionals);
        ret
    }
}

impl Bytable for DNSQuestion {
    fn to_bytes(&self) -> Bytes {
        let mut ret = self.encoded_name.clone();
        append_u16(&mut ret, self.tipe);
        append_u16(&mut ret, self.class);
        ret
    }
}

impl Display for DNSRecord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let ip: String = self
            .data
            .iter()
            .map(|oct| format!("{oct}"))
            .collect::<Vec<_>>()
            .join(".");
        write!(f, "{}: {ip}", &self.name)
    }
}

impl Debug for DNSRecord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{self} (TTL {}, type {}, class {})",
            self.ttl, self.tipe, self.class
        )
    }
}

impl Display for DNSPacket {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let qs = self
            .questions
            .iter()
            .map(|q| String::from_utf8(q.encoded_name.to_vec()).unwrap())
            .collect::<Vec<_>>()
            .join(", ");

        let ans = self
            .answers
            .iter()
            .map(|answer| format!("{answer:?}"))
            .collect::<Vec<_>>()
            .join("\n");

        write!(f, "Questions: {qs}\n\nAnswers:\n{ans}")
    }
}

//-************************************************************************
// Private helpers
//-************************************************************************

#[derive(Debug, Clone)]
struct Reader<'bytes> {
    pub bytes: &'bytes [u8],
    pub cur: usize,
}

impl<'bytes> Reader<'bytes> {
    fn new(bytes: &'bytes [u8]) -> Self {
        Self { bytes, cur: 0 }
    }

    fn seek(&mut self, pos: usize) {
        self.cur = pos.min(self.bytes.len());
    }

    fn next(&mut self) -> Option<u8> {
        let end = self.bytes.len();
        let ret = self.bytes.get(self.cur).copied();
        self.cur = (self.cur + 1).min(end);
        ret
    }

    fn _peek(&self) -> Option<u8> {
        self.bytes.get(self.cur).copied()
    }

    fn take(&mut self, len: usize) -> &[u8] {
        let last = self.bytes.len();
        let end = (self.cur + len).min(last);
        let ret = &self.bytes[self.cur..end];
        self.cur = (self.cur + len).min(last);
        ret
    }

    fn tell(&self) -> usize {
        self.cur
    }
}

//-************************************************************************
// translated directly from the tutorial
//-************************************************************************

fn parse_response(response: &[u8]) -> DNSPacket {
    let iter = &mut Reader::new(response);
    let header = decode_header(iter);
    let num_qs = header.questions;
    let num_ans = header.answers;
    let num_auths = header.authorities;
    let num_adds = header.additionals;

    let mut questions = Vec::with_capacity(num_qs as usize);
    for _ in 0..num_qs {
        questions.push(decode_question(iter));
    }

    let mut answers = Vec::with_capacity(num_ans as usize);
    for _ in 0..num_ans {
        answers.push(decode_record(iter));
    }

    let mut authorities = Vec::with_capacity(num_auths as usize);
    for _ in 0..num_auths {
        authorities.push(decode_record(iter));
    }

    let mut additionals = Vec::with_capacity(num_adds as usize);
    for _ in 0..num_adds {
        additionals.push(decode_record(iter));
    }

    DNSPacket {
        header,
        questions,
        answers,
        authorities,
        additionals,
    }
}

fn decode_header(iter: &mut Reader) -> DNSHeader {
    DNSHeader {
        id: extract_u16(iter),
        flags: extract_u16(iter),
        questions: extract_u16(iter),
        answers: extract_u16(iter),
        authorities: extract_u16(iter),
        additionals: extract_u16(iter),
    }
}

fn decode_question(iter: &mut Reader) -> DNSQuestion {
    DNSQuestion {
        encoded_name: decode_name(iter).into_bytes(),
        tipe: extract_u16(iter),
        class: extract_u16(iter),
    }
}

fn decode_record(iter: &mut Reader) -> DNSRecord {
    let name = decode_name(iter);
    let tipe = extract_u16(iter);
    let class = extract_u16(iter);
    let ttl = extract_u32(iter);
    let len = extract_u16(iter);
    let data = iter.take(len as usize).to_vec();
    DNSRecord {
        name,
        tipe,
        class,
        ttl,
        data,
    }
}

fn encode_name(name: &str) -> Bytes {
    let mut out = Vec::new();
    let name = name.to_ascii_lowercase();
    for part in name.split('.') {
        let part = part.as_bytes();
        let len = (part.len()) as u8;
        out.push(len);
        out.extend_from_slice(part);
    }
    out.push(0u8);

    out
}

fn decode_name(name: &mut Reader) -> String {
    let mut parts = Vec::new();
    while let Some(len) = name.next() {
        if len == 0 {
            break;
        }
        if (len & 0b1100_0000u8) > 0 {
            let part = decode_compressed_name(len, name);
            parts.push(part);
            break;
        }
        let part = String::from_utf8(name.take(len as usize).to_vec()).unwrap();
        parts.push(part);
    }
    parts.join(".")
}

fn decode_compressed_name(len: u8, name: &mut Reader) -> String {
    let offset = u16::from_be_bytes([len & 0b0011_1111, name.next().unwrap()]) as usize;
    let prev = name.tell();
    name.seek(offset);
    let nm = decode_name(name);
    name.seek(prev);

    nm
}

//-************************************************************************
// utility fns
//-************************************************************************

fn append_u16(vec: &mut Vec<u8>, val: u16) {
    for v in val.to_be_bytes() {
        vec.push(v);
    }
}

fn extract_u16(iter: &mut Reader) -> u16 {
    u16::from_be_bytes([iter.next().unwrap(), iter.next().unwrap()])
}

fn extract_u32(iter: &mut Reader) -> u32 {
    u32::from_be_bytes([
        iter.next().unwrap(),
        iter.next().unwrap(),
        iter.next().unwrap(),
        iter.next().unwrap(),
    ])
}

fn _hex(v: &[u8]) -> String {
    let mut out = String::with_capacity(v.len() * 2);
    for n in v.iter() {
        out.push_str(&format!("{n:02x}"));
    }
    out
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn builds_query() {
        let example = "3c5f0100000100000000000003777777076578616d706c6503636f6d0000010001";
        let id = 0x3c5f;
        let query = build_query("www.example.com", TYPE_A, id, RECURSION_DESIRED);
        let qs = _hex(&query);
        assert_eq!(example, qs.as_str());
    }

    #[test]
    fn header_to_from_bytes() {
        let header = DNSHeader {
            id: 70,
            flags: 1,
            questions: 1,
            answers: 1,
            authorities: 2,
            additionals: 5,
        };
        let bytes = header.to_bytes();
        let reader = &mut Reader::new(&bytes);
        let unbytes: DNSHeader = decode_header(reader);

        assert_eq!(header, unbytes);
    }

    #[test]
    fn encode_decode_name() {
        let procben = encode_name("nebcorp.com");
        let mut reader = Reader::new(&procben);
        let nebcorp = decode_name(&mut reader);
        assert_eq!(nebcorp.as_str(), "nebcorp.com");
    }
}
