use rand::{rngs::StdRng, Rng, SeedableRng};
use weekend_dns::*;

fn main() {
    let rng = &mut StdRng::seed_from_u64(2);
    let id = rng.gen();
    let host = std::env::args().last().unwrap_or("nebcorp.com".to_string());
    let query = build_query(&host, TYPE_A, id, RECURSION_DESIRED);
    let packet = send_query(&query, "8.8.8.8");
    println!("{packet}");
}
